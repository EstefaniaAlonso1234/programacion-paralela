program multiplicar
  use omp_lib
  implicit none
  integer,parameter::n=10000,m=10000
  real(kind=8)::sum
  integer::i,j,nthreads=4
  real(kind=8),dimension(n,m)::matriz
  real(kind=8),dimension(m)::vector1
  real(kind=8),dimension(n)::vector2
  call random_number(matriz)
  call random_number(vector1)
  !$ call omp_set_num_threads(nthreads)
  !$omp parallel do private(sum)
  do i=1,n
     sum=0
     do j=1,m
        sum=sum+matriz(i,j)*vector1(j)
     enddo
     vector2(i)=sum
  enddo

  !$omp end parallel do
end program multiplicar
